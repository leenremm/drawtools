# DrawTools

Use this tool to draw floor plans, and then download it as an image file.

## Getting Started

To be viewed in a broswer. Download files, and open index.html in a browser.

### Prerequisites

Tested in the following broswers:

Chrome Version 70.0
Firefox 63.0

## Authors

* **Leen Remmelzwaal** - [leenremm](https://bitbucket.org/leenremm/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
